import math

def solution1(n):
    return findNumber(n, 0)

def findNumber(n, moves):
    n = int(n)
    if n <= 1:
        return moves + 0
    if n % 2 != 0:
        lowestMovesByAdding = findNumber(n + 1, moves + 1)
        lowestMovesBySubtracting = findNumber(n - 1, moves + 1)
        results = [lowestMovesByAdding, lowestMovesBySubtracting]
        results.sort()
        return results[0]
    else:
        return findNumber(n/2, moves + 1)

allMoves = [0,0,1,2,2,3] #answers for 0-3
def solution2(n):
    global allMoves
    n = int(n)
    
    while len(allMoves) - 1 < n:
        if len(allMoves) % 2 != 0:
            allMoves = allMoves + [int(solution2((len(allMoves) + 1) / 2) + 2)]
            if allMoves[-2] + 1 < allMoves[-1]:
                allMoves[-1] = allMoves[-2] + 1
        else:
            allMoves = allMoves + [allMoves[int(len(allMoves) / 2)] + 1]
    surroundingMoves = []
    if n - 1 > 0:
        surroundingMoves = surroundingMoves + [allMoves[n-1] + 1]
    surroundingMoves = surroundingMoves + [allMoves[n]]
    if n + 1 < len(allMoves):
        surroundingMoves = surroundingMoves + [allMoves[n+1] + 1]
    surroundingMoves.sort()
    return surroundingMoves[0]


def solution3(n):
    return find3(n, 0)

def find3(n, moves):
    n = int(n)
    if n <= 1:
        return moves
    if int(n % 2) == 0:
        return find3(n/2, moves + 1)
    if int(n % 59) == 0:
        return find3(n + 1, moves + 1)
    if int(n % 8) == 7:
        return find3(n + 1, moves + 1)
    return find3(n - 1, moves + 1)

def solution4(n):
    n = int(n)
    moves = 0
    while n > 1:
        moves += 1
        if n % 2 == 0:
            n /= 2
        elif n % 8 == 7:
            n += 1
        else:
            n -= 1
    return moves

def solution(n):
    n = int(n)
    moves = 0
    while n > 1:
        moves += 1
        if n % 2 == 0:
            n //= 2
        elif n == 3:
            n -= 1
        elif n % 4 == 1:
            n -= 1
        else:
            n += 1
    return moves

answer = [
     0 #1 
    ,1 #2 
    ,2 #3 
    ,2 #4
    ,3 #5 
    ,3 #6 
    ,4 #7-----------= 
    ,3 #8 
    ,4 #9 
    ,4 #10
    ,5 #11
    ,4 #12
    ,5 #13
    ,5 #14
    ,5 #15-----------
    ,4 #16
    ,5 #17
    ,5 #18
    ,6 #19
    ,5 #20
    ,6 #21
    ,6 #22
    ,6 #23-----------
    ,5 #24
    ,6 #25
    ,6 #26
    ,7 #27
    ,6 #28
    ,7 #29
    ,6 #30
    ,6 #31-----------
    ,5 #32
    ,6 #33
    ,6 #34
    ,7 #35
    ,6 #36
    ,7 #37
    ,7 #38
    ,7 #39-----------
    ,6 #40
    ,7 #41
    ,7 #42
    ,8 #43
    ,7 #44
    ,8 #45
    ,7 #46
    ,7 #47-----------
    ,6 #48
    ,7 #49
    ,7 #50
    ,8 #51
    ,7 #52
    ,8 #53
    ,8 #54
    ,8 #55-----------
    ,7 #56
    ,8 #57
    ,8 #58
    ,8 #59 xxxxxxxxxxxxxxxxxxxxxx
    ,7 #60
    ,8 #61
    ,7 #62
    ,7 #63-----------
    ,6 #64
    ]
print(solution(456541354165876121897463213674859845623135497808640216340890780231654890021350684432103216850794632103268749630121320898701))
print(solution3(456541354165876121897463213674859845623135497808640216340890780231654890021350684432103216850794632103268749630121320898701))
# print(solution('51')) 
# answer = [0] + answer
# print(answer)  
# print(allMoves)  
# for i in range(len(answer)):
#     print('i: ', (i + 1), solution(str(i + 1)), solution(str(i + 1)) == answer[i])